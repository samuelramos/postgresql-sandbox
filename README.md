# PostgreSQL Sandbox

## Setup using Docker containers

### Create a Docker network

```
$ docker network create postgresql-sandbox-network
```

## Run a PostgreSQL container

Running the image with `-d` runs the container in detached mode, leaving the container running in the background. The `-p` flag redirects a public port to a private port inside the container. 

```
$ docker run -d \
    -p 5432:5432 \
    --name postgresql-sandbox \
    -e POSTGRES_DB=postgres \
    -e POSTGRES_PASSWORD=passw0rd \
    -v $(pwd)/app-data/server:/var/lib/postgresql/data \
    --net postgresql-sandbox-network \
    postgres:12
```

## Run a pgadmin 4 container

Running the image with `-d` runs the container in detached mode, leaving the container running in the background. The `-p` flag redirects a public port to a private port inside the container. 

```
docker run -p 80:80 \
    -e 'PGADMIN_DEFAULT_EMAIL=samuelramos015@gmail.com' \
    -e 'PGADMIN_DEFAULT_PASSWORD=p4ssword' \
    -e 'PGADMIN_CONFIG_ENHANCED_COOKIE_PROTECTION=True' \
    -e 'PGADMIN_CONFIG_LOGIN_BANNER="Authorised users only!"' \
    -e 'PGADMIN_CONFIG_CONSOLE_LOG_LEVEL=10' \
    -v $(pwd)/app-data/pgadmin:/var/lib/pgadmin \
    --net postgresql-sandbox-network \
    -d dpage/pgadmin4
```

## References

Docker Hub: postgres 

From <https://hub.docker.com/_/postgres>

Docker Hub: pgadmin 4

From <https://hub.docker.com/r/dpage/pgadmin4>

\[pgadmin\] Container Deployment

From <https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html>